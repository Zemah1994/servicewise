public with sharing class WorkingHourController {
    public WorkingHourController() {
        
    }

    @AuraEnabled(cacheable=true)
    public static Map<String,String> getAllContactsExcept(String id){
        Map<String,String> retval = new Map<String,String>();
        for(Contact cObj: [Select id,Name From Contact WHERE Id != :id]) {
            retval.put(cObj.Id, cObj.Name);
        }
        return retval;
    }

    @AuraEnabled(cacheable=true)
    public static Working_Hour__c getRecord(string recordId) {
        list<Working_Hour__c> queryResult =
         [Select Id, Date__c, Contact__c,Account__c,AmountOfHours__c,Name From Working_Hour__c WHERE Id = :recordId];
        Working_Hour__c retval = null;
        // if(queryResult.size() != 0) {
            retval = queryResult[0];
        // }
        return  retval;
    }

    @AuraEnabled
    public static Working_Hour__c crateRecord(Working_Hour__c record) {
        Working_Hour__c retval = null;
        if(record != null) {
            insert record;
            retval =  record;
        }
        return retval;
    }
    @AuraEnabled
    public static void removeRecord(Working_Hour__c record) {
        if(record != null) {
            try {
                delete record;
            } catch (DmlException e) {
                // Process exception here
            }
        }
    }

}