import { LightningElement, track, api, wire } from "lwc";

import getAllContactsExcept from "@salesforce/apex/WorkingHourController.getAllContactsExcept";
import getRecord from "@salesforce/apex/WorkingHourController.getRecord";
import crateRecord from "@salesforce/apex/WorkingHourController.crateRecord";
import removeRecord from "@salesforce/apex/WorkingHourController.removeRecord";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { NavigationMixin } from "lightning/navigation";

export default class WorkingHours extends NavigationMixin(LightningElement) {
    @api recordId;
    @track currentRecord;
    contactList;
    selectedContactId;
    @wire(getAllContactsExcept, { id: "$currentRecord.Id" })
    allContacts({ error, data }) {
        let tmpArray = [];
        if (data) {
            for (let key in data) {
                tmpArray.push({ label: data[key], value: key });
            }
            this.contactList = tmpArray;
        }
    }

    currentRecord = null;

    // retrieved the record from database thet should change
    @wire(getRecord, { recordId: "$recordId" })
    setCurrentRecord({ error, data }) {
        if (data) {
            this.currentRecord = data;
        }
    }

    handleSave() {
        if (this.selectedContactId == null) {
            this.showToastEvent("Error", "Please select a contact", "error");
        } else {
            this.swap();
        }
    }

    swap() {
        // change contact and reset Id(for insert)
        let whRecord = JSON.parse(JSON.stringify(this.currentRecord));
        whRecord.Contact__c = this.selectedContactId;
        whRecord.Id = null;
        // insert the changed record to DB
        crateRecord({ record: whRecord }).then((result) => {
            // after the new record was created it is possible to get it
            this.navigateToNewRecordPage(result);
        });

        // remove the old record from DB
        removeRecord({ record: this.currentRecord });

        //show success message
        this.showToastEvent(
            "Success",
            "The contact was successfully changed",
            "success"
        );
        // close window
        this.closeQuickAction();
        // });
    }

    handleContactChange(event) {
        this.selectedContactId = event.target.value;
    }

    closeQuickAction() {
        this.dispatchEvent(new CustomEvent("close"));
    }

    showToastEvent(title, message, variant) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: message,
                variant: variant
            })
        );
    }

    navigateToNewRecordPage(record) {
        this[NavigationMixin.Navigate]({
            type: "standard__recordPage",
            attributes: {
                recordId: record.Id,
                actionName: "view"
            }
        });
    }
}